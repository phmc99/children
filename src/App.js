import "./App.css";
import { ChildrenElt } from "./components/children";

function App() {
  return (
    <div className="App">
      <div className="App-header">
        <ChildrenElt>
          <h1>Teste</h1>
        </ChildrenElt>
        <ChildrenElt>
          <h1>Teste</h1>
        </ChildrenElt>
        <ChildrenElt>
          <h1>Teste</h1>
        </ChildrenElt>
      </div>
    </div>
  );
}

export default App;
